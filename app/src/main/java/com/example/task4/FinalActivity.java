package com.example.task4;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class FinalActivity extends AppCompatActivity{
    TextView name1,phone1,date1,section1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activit2);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        name1 = (TextView)findViewById(R.id.text);
        phone1= (TextView)findViewById(R.id.text1);
        date1 = (TextView)findViewById(R.id.text2);
        section1 = (TextView)findViewById(R.id.text3);

        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        String phone = intent.getStringExtra("phone");
        String Date = intent.getStringExtra("Date");
        String Section = intent.getStringExtra("Section");
        name1.setText(name);
        phone1.setText(phone);
        date1.setText(Date);
        section1.setText(Section);
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }


}
