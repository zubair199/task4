package com.example.task4;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.content.Intent;
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText _name = (EditText) findViewById(R.id.Text1);
        final EditText _phone = (EditText) findViewById(R.id.Text2);
        final EditText _bod = (EditText) findViewById(R.id.Text3);
        final EditText _sec = (EditText) findViewById(R.id.Text4);

        Button email = (Button) findViewById(R.id.button1);
        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Name = _name.getText().toString();
                String Phone = _phone.getText().toString();
                String Birth = _bod.getText().toString();
                String section = _sec.getText().toString();
                if (TextUtils.isEmpty(Name)) {
                    _name.setError("Enter Your Name");
                    _name.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(Phone)) {
                    _phone.setError("Enter Your Phone");
                    _phone.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(Birth)) {
                    _bod.setError("Enter Your Date of Birth");
                    _bod.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(section)) {
                    _sec.setError("Enter Your Section");
                    _sec.requestFocus();
                    return;
                }


                Intent intent = new Intent(getApplicationContext(), FinalActivity.class);
                intent.putExtra("name", Name);
                intent.putExtra("phone",Phone);
                intent.putExtra("Date",Birth);
                intent.putExtra("Section",section);
                startActivity(intent);
            }

        });


    }

}
